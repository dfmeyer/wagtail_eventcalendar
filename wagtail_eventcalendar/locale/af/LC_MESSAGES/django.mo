��    #      4  /   L           	          !     1  
   D      O     p     y     �  -   �     �     �     �        0        >     O     ^     m  -   }     �     �     �     �     �               3     G  4   V     �     �  ,   �  )   �  k       t     �     �     �     �  )   �  	         
       2   &  
   Y     d     �     �  >   �     �     	     "	     7	  1   M	     	     �	     �	     �	     �	     �	  $   �	     	
     
  4   -
     b
     x
  (   �
  *   �
     
                           !   #                                             	                                            "                                 Calendar Calendar Event Calendar Events Calendar Root Page Categories Categories this event belongs to Category Category name Default Image Default image to be used for calendar entries Description Description of event Description of the calendar End of Event End time of event. Does not need to be same day. Event Categories Event Category Event Finished Event has begun Get entire calendar. Includes all categories. Get the entire calendar for:  Image Location Location of event No description available Parent category Parent category cannot be self. Problem Description Problem Status Start date and time must be before end date and time Start of Event Starting time of event Text that describes the problem. Keep brief. Whether there is a problem with the event Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
                                          Kalender Gebeurtenis Kalender Gebeurtenis Kalender Gebeurtenise Kalendar Vader Bladsy Kategorieë Kategorie waaraan die gebeurtenis behoort Kategorie Kategorie naam Verstek Foto Verstek foto wat vir die kalendar sal gebruik word Beskrywing Beskrywing van die gebeurtenis Beskrywing van die kalendar Einde van Gebeurtenis Eind tyd van die gebeurtenis. Hoof nie selfde dag te wees nie. Gebeurtenis Kategorieë Gebeurtenis Kategorie Gebeurtenis is klaar Gebeurtenis het begin Kry die hele kalender. Sluit alle kategorieë in. Kry die hele kalender vir:  Foto Plek Plek van gebeurtenis Geen beskrywing beskikbaar Ouer kategorie Ouer kategorie kan nie self wees nie Probleem Beskrywing Probleem Status Begin tyd en datum moet voor einde tyd en datum wees Begin van Gebeurtenis Begin tyd van die gebeurtenis Teks wat die probleem beskryf. Hou kort. Of daar 'n probleem is met die gebeurtenis 
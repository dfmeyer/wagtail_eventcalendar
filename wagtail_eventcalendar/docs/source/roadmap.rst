.. _roadmap-label:

==========================
Proposed Roadmap
==========================

0.1
   First release
0.1.5 (End of November 2018)
   Have good test coverage
0.2 (1st week of December 2018)
   Make template tags so that customising and using in own templates is far easier
0.3 (Mid-January 2019)
   Add recurring events functionality
0.4 (End-January 2019)
   Making the urls i18n compliant
0.5 (End-February 2019)
   Implementing permission controls so only certain users can view calendars and entries
0.6 (July 2019)
   Implement sync to caldav and gcal